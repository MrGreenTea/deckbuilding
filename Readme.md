import docker compose run command
```
NEO4J_AUTH="neo4j/passwort" docker-compose -f docker-compose.yml -f docker-compose.dev.yml run --rm graph neo4j-admin import --multiline-fields=true --nodes=/import/colors.csv --nodes=/import/cards.csv --nodes=/import/formats.csv --nodes=/import/subtypes.csv --nodes=/import/types.csv --nodes=/import/sets.csv --relationships=/import/card_relations.csv --relationships=/import/color_relations.csv --relationships=/import/subtype_relations.csv --relationships=/import/type_relations.csv --relationships=/import/legalities.csv --relationships=/import/set_relations.csv
```

model search.
```
CALL db.index.fulltext.queryNodes($index, $search) YIELD node,score
WHERE node.lang="en" AND NOT (node)-[:IS]-(:Type {name: "Token"}) AND (node)--(:Set {digital:False})
WITH node, score
ORDER BY node.released_at DESC
RETURN head(collect([%s])) as h, score
ORDER BY score DESC
LIMIT $limit
```
we exclude tokens, non-english cards and cards from digital sets.
Because we only want to return one object per card (unique by name) and neo4j does not allow to use DISTINCT(node.prop) while also returning the whole node or even more properties, we need to do the hack with head(collect([node.prop1, node.prop2])) which will group the nodes by prop1.
