import os

import dynaconf
import flask
import flask.cli
import flask_cors
import neo4j.exceptions
import sentry_sdk
import werkzeug.exceptions
from flask_jwt_extended import JWTManager
from sentry_sdk.integrations.flask import FlaskIntegration

import auth
import db
import models
import utils

if os.environ["FLASK_ENV"] != "development" or os.environ.get("SENTRY_DSN"):
    sentry_sdk.init(
        dsn=os.environ["SENTRY_DSN"],
        environment=os.environ["FLASK_ENV"],
        integrations=[FlaskIntegration()],
    )

APP = flask.Flask(__name__)
flask_cors.CORS(APP)
dynaconf.FlaskDynaconf(APP)
neo4j_database = db.setup_database(APP)
JWTManager(APP)


@APP.route("/autocomplete")
def autocomplete():
    # TODO fix duplicate Dragon Engine entries
    try:
        query = "~ ".join(flask.request.args["query"].split()) + "~"
        response = models.Card.search(
            search=query,
            limit=flask.request.args.get("limit", 10),
            index="cards",
            properties=["name", "scryfall_id"],
        )
    except (KeyError, neo4j.exceptions.ClientError):
        return werkzeug.exceptions.BadRequest("invalid search query.")

    APP.logger.info("%s", response)
    return flask.jsonify(response)


@APP.route("/card/<scryfall_id>")
def card(scryfall_id):
    try:
        found_card = models.Card.nodes.first(scryfall_id=scryfall_id)
    except models.Card.DoesNotExist:
        return werkzeug.exceptions.NotFound()

    return flask.jsonify(found_card.serialize())


@APP.route("/login", methods=["POST"])
@utils.json_view
def login(username, password):
    user = auth.login(username, password)
    user.authenticated = True

    return auth.get_access_token(user)


@APP.route("/register", methods=["POST"])
@utils.json_view
def register(username, email, password, password_repeat):
    new_user = auth.register(
        email=email,
        username=username,
        password=password,
        password_repeat=password_repeat,
    )
    return new_user.serialize()


@APP.route("/decks/<name>", methods=["GET"])
def get_deck(name):
    try:
        return flask.jsonify(models.Deck.nodes.get(name=name).serialize())
    except models.Deck.DoesNotExist:
        return werkzeug.exceptions.NotFound()


@APP.route("/decks/<name>", methods=["POST"])
def new_deck(name):
    try:
        created_deck = models.Deck(name=name)
        created_deck.save()
        created_deck.format.connect(models.Format.nodes.get(name="commander"))  # TODO
    except neo4j.exceptions.ConstraintError:
        return werkzeug.exceptions.Conflict("A deck with this name already exists.")

    return flask.jsonify(created_deck.serialize())


@APP.route("/decks/<name>/add", methods=["POST"])
@utils.json_view
def add_cards_to_deck(name, cards=None):
    if not cards:
        return flask.Response(status=304)

    with db.database.transaction:
        deck = models.Deck.nodes.get(name=name)
        for scryfall_id in cards:
            deck.cards.connect(models.Card.nodes.first(scryfall_id=scryfall_id))
        deck.refresh()
    return deck.serialize()


@APP.route("/decks/<name>/remove", methods=["POST"])
@utils.json_view
def remove_cards_from_deck(name, cards=None):
    if not cards:
        return flask.Response(status=304)

    with db.database.transaction:
        deck = models.Deck.nodes.get(name=name)
        for scryfall_id in cards:
            deck.cards.disconnect(models.Card.nodes.first(scryfall_id=scryfall_id))
        deck.refresh()
    return deck.serialize()


APP.cli.add_command(db.database_cli)

if __name__ == "__main__":
    APP.run()
