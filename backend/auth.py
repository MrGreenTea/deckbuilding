import flask_jwt_extended
import neomodel as ogm
import werkzeug.exceptions
from passlib.context import CryptContext

from utils import SerializableNodeMixin

pwd_context = CryptContext(schemes=["argon2"])


class User(ogm.StructuredNode, SerializableNodeMixin):
    serialze_only = ("username", "email", "is_active")

    username = ogm.StringProperty(unique_index=True, required=True)
    email = ogm.StringProperty(unique_index=True, required=True)
    password = ogm.StringProperty(required=True)
    is_active = ogm.BooleanProperty(default=True)

    authenticated = False

    decks = ogm.RelationshipTo("models.Deck", "CREATED")


class NotAuthenticated(werkzeug.exceptions.Unauthorized):
    pass


class WrongPassword(NotAuthenticated):
    pass


class WrongUsername(NotAuthenticated, User.DoesNotExist):
    pass


def register(email, username, password, password_repeat):
    if password == password_repeat:
        password_hash = pwd_context.hash(password)
        new_user = User(username=username, email=email, password=password_hash).save()
        return new_user

    raise WrongPassword


def change_password(username, old_password, new_password):
    user = User.get(username=username)
    if pwd_context.verify(old_password, user.password):
        user.password = pwd_context.hash(new_password)
        user.save()
        return user

    raise WrongPassword


def login(username, password):
    try:
        user = User.nodes.get(username=username)
    except User.DoesNotExist:
        pass
    else:
        if pwd_context.verify(password, user.password):
            return user

    raise NotAuthenticated


def get_access_token(user):
    if user.authenticated:
        return flask_jwt_extended.create_access_token(identity=user.username)

    raise NotAuthenticated
