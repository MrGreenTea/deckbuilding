import csv
import json
import pathlib
import socket
import typing

import backoff
import click
import flask
import neomodel
import requests
import tqdm
import yurl
from flask import current_app
from flask.cli import with_appcontext
from werkzeug.local import LocalProxy

import models


def wait_for(uri, service, *, timeout=15, app=current_app):
    """
    Wait for uri to come online in max timeout seconds.
    service arg is used for logging.
    app defaults to current_app
    """
    parsed = yurl.URL(uri)

    def log_backoff(data):
        if data["tries"] == 1:
            app.logger.info("Waiting for %s at %r to come online.", service, uri)

    def log_success(data):
        if data["tries"] > 1:
            app.logger.info("%s online after %i seconds.", service, data["elapsed"])

    def log_giveup(data):
        app.logger.error(
            "%s connection not possible after %i seconds", service, data["elapsed"]
        )

    @backoff.on_exception(
        backoff.expo,
        (socket.error, ConnectionError),
        on_backoff=log_backoff,
        on_success=log_success,
        on_giveup=log_giveup,
        max_time=timeout,
    )
    def check():
        socket.create_connection((parsed.host, parsed.port), timeout=1).close()

    return check()


def setup_database(app):
    wait_for(uri=app.config["NEO4J_URL"], service="database", timeout=30, app=app)
    parsed_uri = yurl.URL(app.config["NEO4J_URL"])
    if app.config["NEO4J_AUTH"]:
        if parsed_uri.userinfo:
            app.logger.warning("NEO4J_AUTH will override auth in NEO4J_URL.")
        user, _, password = app.config["NEO4J_AUTH"].partition("/")
        parsed_uri = parsed_uri.replace(userinfo=":".join((user, password)))

    url = parsed_uri.as_string()
    neomodel.config.DATABASE_URL = url
    neomodel.db.set_connection(url)
    return neomodel.db


def get_db() -> neomodel.Database:
    db = setup_database(current_app)
    if "db" not in flask.g:
        flask.g.db = db

    return flask.g.db


database = LocalProxy(get_db)
database_cli = flask.cli.AppGroup("database")


@database_cli.command("export")
@click.argument(
    "save_to", default="/export", type=click.Path(file_okay=False, writable=True)
)
@with_appcontext
def export_scryfall_to_csv(save_to):

    request = requests.get("https://api.scryfall.com/bulk-data")
    request.raise_for_status()
    set_request = requests.get("https://api.scryfall.com/sets/")
    set_request.raise_for_status()

    bulk_data = request.json()
    default_cards = next(
        info for info in bulk_data["data"] if info["type"] == "default_cards"
    )
    save_to = pathlib.Path(save_to) / default_cards["updated_at"]
    save_to.mkdir(parents=True, exist_ok=True)
    (save_to / "nodes").mkdir()
    (save_to / "relations").mkdir()
    file = save_to / "data.json"

    if not file.exists():
        click.echo(f"downloading new bulk data to {file}")
        request = requests.get(default_cards["permalink_uri"], allow_redirects=True)
        request.raise_for_status()
        data = request.json()
        file.parent.mkdir(exist_ok=True)
        file.write_bytes(request.content)
        click.echo("Download and parsing complete.")
    else:
        click.echo(f"Read from {file}")
        with file.open() as json_file:
            data = json.load(json_file)

    all_types = set()
    all_subtypes = set()
    all_formats = set()

    def extract_types(type_line: str):
        nonlocal all_types, all_subtypes

        front, _, back = type_line.partition("//")

        front_types, _, front_sub_types = front.partition("—")
        back_types, _, back_sub_types = front.partition("—")

        card_types = set(front_types.split() + back_types.split())
        card_sub_types = set(front_sub_types.split() + back_sub_types.split())

        all_types |= card_types
        all_subtypes |= card_sub_types

        return list(card_types), list(card_sub_types)

    def card(props: dict):
        for k in ["multiverse_ids", "games", "related_uris", "color_indicator"]:
            props.pop(k, None)  # ignore non existing keys

        for size, uri in props.pop("image_uris", {}).items():
            props[f"image_{size}"] = uri

        legalities = props.pop("legalities", {})
        for mtg_format in legalities:
            all_formats.add(mtg_format)

        card_types, card_sub_types = extract_types(props["type_line"])
        rels = {
            "colors": props.pop("colors", []),
            "identity": props.pop("color_identity", []),
            "parts": props.pop("all_parts", []),
            "types": card_types,
            "sub_types": card_sub_types,
            "legalities": legalities,
            "card_faces": props.pop("card_faces", []),
            "set": props["set"],
        }

        assert all(
            type(v) in {int, bool, str, float} for v in props.values()
        ), f"{props} still contains invalid value types"

        props["scryfall_id"] = props.pop("id")
        return {"props": props, "rels": rels}

    model_ids = {
        models.Card: "scryfall_id",
        models.Format: "name",
        models.SubType: "name",
        models.Type: "name",
        models.Color: "abbr",
        models.Set: "code",
    }

    def model_to_csv_header(model: typing.Type[neomodel.StructuredNode]):
        assert model in model_ids

        def add_type(prop, name):
            suffix = "string"
            if name == model_ids[model]:
                suffix = f"ID({model.__name__})"
            elif isinstance(prop, neomodel.BooleanProperty):
                suffix = "boolean"
            elif isinstance(prop, neomodel.IntegerProperty):
                suffix = "int"
            elif isinstance(prop, neomodel.FloatProperty):
                suffix = "float"
            elif isinstance(prop, neomodel.DateProperty):
                suffix = "date"
            return f"{name}:{suffix}"

        prop_fields = {
            name: add_type(prop, name) for name, prop in model.__all_properties__
        }
        prop_fields[":LABEL"] = ":LABEL"
        return prop_fields

    # filter empty cards (for now only from not being legal in any format at all)
    cards_to_import = [
        c for c in map(card, tqdm.tqdm(data, desc="Processing card data.")) if c
    ]

    def write_models(model: typing.Type[neomodel.StructuredNode], suffix, values):
        to_path = save_to / "nodes" / suffix
        label = ";".join(model.inherited_labels())
        model_fields = model_to_csv_header(model)

        def fields(f):
            fields_ = {
                model_fields[k]: (str(v).lower() if isinstance(v, bool) else v)
                for k, v in f.items()
                if k in model_fields
            }
            return {":LABEL": label, **fields_}

        with to_path.open("w") as csv_file:
            writer = csv.DictWriter(
                csv_file,
                fieldnames=list(model_fields.values()),
                quotechar='"',
                quoting=csv.QUOTE_ALL,
            )
            writer.writeheader()
            writer.writerows(
                (
                    fields(v)
                    for v in tqdm.tqdm(
                        values, desc=f"Writing csv for {model.__qualname__}"
                    )
                )
            )

    write_models(models.Format, "formats.csv", [{"name": f} for f in all_formats])
    write_models(models.Type, "types.csv", [{"name": t} for t in all_types])
    write_models(models.SubType, "subtypes.csv", [{"name": t} for t in all_subtypes])
    write_models(models.Card, "cards.csv", (c["props"] for c in cards_to_import))

    def id_to_scryfall_id(props):
        props["scryfall_id"] = props.pop("id")
        return props

    all_sets = [id_to_scryfall_id(d) for d in set_request.json()["data"]]

    write_models(models.Set, "sets.csv", all_sets)

    unprocessed_relations = {}
    legal_relations = []

    all_relations = {"Color": [], "Card": [], "Type": [], "SubType": [], "Set": []}

    for card in tqdm.tqdm(cards_to_import):
        props, relations = card["props"], card["rels"]
        card_id = props[model_ids[models.Card]]

        def relate_to(end_id, end_namespace, rel_type, *, swap=False, **extra_data):
            start, end = ":START_ID", ":END_ID"
            if swap:
                start, end = end, start

            start = f"{start}({models.Card.__name__})"
            end = f"{end}({end_namespace})"

            return {start: card_id, end: end_id, ":TYPE": rel_type, **extra_data}

        all_relations["Color"].extend(
            relate_to(color, "Color", "COLOR") for color in relations.pop("colors")
        )
        all_relations["Color"].extend(
            relate_to(color_id, "Color", "IDENTITY")
            for color_id in relations.pop("identity")
        )
        all_relations["Card"].extend(
            relate_to(
                part["id"], "Card", "PART_OF", swap=True, component=part["component"]
            )
            for part in relations.pop("parts")
            if part["name"] != props["name"] and part["component"] != "combo_piece"
        )
        all_relations["Type"].extend(
            relate_to(card_type, "Type", "IS") for card_type in relations.pop("types")
        )
        all_relations["SubType"].extend(
            relate_to(sub_type, "SubType", "IS")
            for sub_type in relations.pop("sub_types")
        )
        all_relations["Set"].append(relate_to(relations.pop("set"), "Set", "IN"))

        legal_rel_map = {"legal": "ALLOWS", "banned": "BANS", "restricted": "RESTRICTS"}
        legal_relations.extend(
            relate_to(format_name, "Format", legal_rel_map[legal], swap=True)
            for format_name, legal in relations.pop("legalities").items()
            if legal != "not_legal"
        )
        unprocessed_relations |= relations.keys()

    for relation, rows in all_relations.items():
        with (save_to / "relations" / relation.lower()).with_suffix(".csv").open(
            "w"
        ) as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=list(rows[0].keys()))
            writer.writeheader()
            writer.writerows(rows)

    if unprocessed_relations:
        click.echo(
            "Warning: relations {} where not processed!".format(
                ",".join(unprocessed_relations)
            )
        )

    legal_csv = save_to / "relations/legalities.csv"
    click.echo(f"Creating legality relations csv to {legal_csv}")
    with legal_csv.open("w") as csv_file:
        writer = csv.DictWriter(
            csv_file, fieldnames=[":START_ID(Format)", ":END_ID(Card)", ":TYPE"]
        )
        writer.writeheader()
        writer.writerows(
            sorted(legal_relations, key=lambda l: (l[":START_ID(Format)"], l[":TYPE"]))
        )

    click.echo("Copying color.csv")
    from shutil import copyfile

    colors = pathlib.Path(current_app.root_path) / "data/colors.csv"
    color_target = save_to / "nodes/colors.csv"
    click.echo(f"Copying color nodes csv to {color_target}")
    copyfile(colors, color_target)

    click.echo("Import completed.")


@database_cli.command("import")
@click.argument("load_from", default="/export/", type=click.Path(file_okay=False))
def import_neo4j_admin(load_from):
    import os.path

    load_from = pathlib.Path(load_from).iterdir()
    newest_folder = max(load_from, key=os.path.getctime)
    nodes = " ".join(f"--nodes={f}" for f in (newest_folder / "nodes").iterdir())
    relations = " ".join(
        f"--relationships={f}" for f in (newest_folder / "relations").iterdir()
    )
    command = "neo4j-admin import --multiline-fields=true {nodes} {relations}".format(
        nodes=nodes, relations=relations
    )
    print(command)


@database_cli.command("init")
@with_appcontext
def init_db():
    graph = get_db()

    click.echo("Clean all indices.")
    results, _ = graph.cypher_query(
        'CALL db.indexes() YIELD indexName WHERE indexName = "card" RETURN indexName'
    )
    if results:
        graph.cypher_query('CALL db.index.fulltext.drop("cards")')
    neomodel.remove_all_labels()

    click.echo("Create card fulltext index.")
    graph.cypher_query(
        'CALL db.index.fulltext.createNodeIndex("cards",["Card"],["oracle_text", "name", "flavor_text"])'
    )

    neomodel.install_all_labels()
    click.echo("Created constraints.")
