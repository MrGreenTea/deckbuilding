import typing
from datetime import date

import neomodel as ogm
from flask import current_app

import db
from utils import SerializableNodeMixin


class SearchMixin:
    # this query grew quite organically. Check the readme for more info about it.
    SEARCH_QUERY = r"""
    CALL db.index.fulltext.queryNodes($index, $search) YIELD node,score
    WHERE node.lang="en" AND NOT (node)-[:IS]-(:Type {name: "Token"}) AND (node)--(:Set {digital:False})
    WITH node, score
    ORDER BY node.released_at DESC
    RETURN head(collect([%s])) as h, score
    ORDER BY score DESC
    LIMIT $limit
    """

    REGEX_QUERY = r"""
    MATCH (node:{type})
    WHERE not (node)-[:IS]-(:Type {{name: "Token"}}) AND node.{prop} =~ $search
    RETURN node.{prop}
    ORDER BY SIZE(node.{prop})
    LIMIT $limit
    """

    @classmethod
    def search(
        cls: typing.Type[ogm.StructuredNode],
        index,
        search: str,
        limit: int = 10,
        property_name=None,
        properties=None,
    ):
        if properties is None:
            properties = ["name"]
        cypher_query = cls.SEARCH_QUERY % ",".join(f"node.{p}" for p in properties)
        graph = db.database

        if property_name is not None:
            search = f'{property_name}:"{search}"'

        params = {"search": search, "limit": limit, "index": index}
        current_app.logger.debug(
            "Running cypher query %r with params %r", cypher_query, params
        )

        results, _ = graph.cypher_query(query=cypher_query, params=params)
        # cypher_query returns a list of lists for each result...
        return [dict(zip(properties, r[0])) for r in results]

    @classmethod
    def re_search(cls, search: str, property_name, limit: int = 10):
        cypher_query = cls.REGEX_QUERY.format(type=cls.__name__, prop=property_name)
        graph = db.get_db()

        search = r"\W+".join(search.split())

        params = {"search": search, "limit": limit}
        current_app.logger.debug(
            "Running cypher query %r with params %r", cypher_query, params
        )

        results, _ = graph.cypher_query(
            query=cypher_query, params=params, resolve_objects=True
        )

        # cypher_query returns a list of lists for each result...
        return results


class ISODateProperty(ogm.DateProperty):
    def deflate(self, value, *args, **kwargs):
        """Because parent deflate has validator decorator it's signature changes."""
        date.fromisoformat(value)  # check that date is in ISO format.
        return value


class Color(ogm.StructuredNode):
    abbr = ogm.StringProperty(unique_index=True)
    name = ogm.StringProperty(unique_index=True)
    basic = ogm.StringProperty()


class Type(ogm.StructuredNode):
    name = ogm.StringProperty(unique_index=True, required=True)


class SubType(ogm.StructuredNode):
    name = ogm.StringProperty(unique_index=True, required=True)


class Set(ogm.StructuredNode):
    code = ogm.StringProperty(unique_index=True)
    scryfall_id = ogm.StringProperty(unique_index=True)
    name = ogm.StringProperty(unique_index=True)
    uri = ogm.StringProperty()
    scryfall_uri = ogm.StringProperty()
    released_at = ISODateProperty()
    set_type = ogm.StringProperty()
    digital = ogm.BooleanProperty()
    foil_only = ogm.BooleanProperty()
    icon_svg_uri = ogm.StringProperty()
    block_code = ogm.StringProperty()
    block = ogm.StringProperty()

    parent = ogm.RelationshipTo("Set", "PARENT")


class Card(ogm.StructuredNode, SerializableNodeMixin, SearchMixin):
    scryfall_id = ogm.StringProperty(unique_index=True)
    name = ogm.StringProperty(index=True)
    oracle_id = ogm.StringProperty(index=True)
    rarity = ogm.StringProperty(index=True)
    image_small = ogm.StringProperty()
    image_normal = ogm.StringProperty()
    image_large = ogm.StringProperty()
    image_png = ogm.StringProperty()
    scryfall_uri = ogm.StringProperty()
    rulings_uri = ogm.StringProperty()
    uri = ogm.StringProperty()
    type_line = ogm.StringProperty()
    oracle_text = ogm.StringProperty(index=False)
    released_at = ISODateProperty()
    cmc = ogm.FloatProperty()
    mana_cost = ogm.StringProperty()
    edhrec_rank = ogm.IntegerProperty()
    lang = ogm.StringProperty()
    flavor_text = ogm.StringProperty()

    colors = ogm.RelationshipTo(Color, "COLOR")
    identity = ogm.RelationshipTo(Color, "IDENTITY")
    types = ogm.RelationshipTo(Type, "IS")
    sub_types = ogm.RelationshipTo(SubType, "IS")
    parts = ogm.RelationshipFrom("Card", "PART_OF")

    set = ogm.RelationshipTo(Set, "IN")
    decks = ogm.RelationshipTo("Deck", "PLAYED_IN")


class Format(ogm.StructuredNode):
    name = ogm.StringProperty(unique_index=True)

    legal = ogm.RelationshipTo(Card, "ALLOWS")
    banned = ogm.RelationshipTo(Card, "BANS")
    restricted = ogm.RelationshipTo(Card, "RESTRICTS")


class Deck(ogm.StructuredNode):
    name = ogm.StringProperty(unique_index=True)
    cards = ogm.RelationshipTo(Card, "PLAYS")
    creator = ogm.RelationshipFrom("auth.User", "CREATED")
    format = ogm.RelationshipTo(Format, "FOR", cardinality=ogm.One)

    def serialize(self):
        return {"name": self.name, "cards": [c.serialize() for c in self.cards]}
