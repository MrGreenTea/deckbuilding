import functools
import inspect

import flask
import neomodel as ogm
import werkzeug.exceptions


def json_error(error: werkzeug.exceptions.HTTPException):
    return flask.jsonify({"code": error.code, "description": error.description})


def json_view(f):
    spec = inspect.getfullargspec(f)
    kwonly = set(spec.kwonlyargs or {}) - set(spec.kwonlydefaults or {})
    pos_args = set(spec.args[: -len(spec.defaults or []) or None])
    needed_args = pos_args | kwonly

    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        if not flask.request.is_json:
            return werkzeug.exceptions.UnsupportedMediaType("Only JSON is supported")

        json_args = needed_args - kwargs.keys()
        from_json_args = flask.request.json
        missing_args = json_args - from_json_args.keys()
        if missing_args:
            return json_error(
                werkzeug.exceptions.UnprocessableEntity(
                    "Missing parameters: {}".format(",".join(missing_args))
                )
            )

        try:
            result = f(*args, **kwargs, **from_json_args)
        except werkzeug.exceptions.HTTPException as error:
            result = error
        if isinstance(result, werkzeug.exceptions.HTTPException):
            return json_error(result)
        return flask.jsonify(result)

    return wrapped


class SerializableNodeMixin:
    serialize_exclude = ()
    serialze_only = None

    def serialize(self: ogm.StructuredNode):
        properties = dict(self.__all_properties__)
        fields_to_serialize = (
            self.serialze_only or properties.keys() - self.serialize_exclude
        )
        return {k: v for k, v in self.__dict__.items() if k in fields_to_serialize}


def is_serializable(obj):
    return hasattr(obj, "serialize")
