import Vue from 'vue'
import './plugins/vuetify';
import * as Integrations from '@sentry/integrations';
import * as Sentry from '@sentry/browser';

import App from './App.vue'
import router from './router'
import store from './store'


if (process.env.VUE_APP_SENTRY_DSN) {
    Sentry.init({
        dsn: process.env.VUE_APP_SENTRY_DSN,
        integrations: [
            new Integrations.Vue({
                Vue,
                attachProps: true,
            }),
        ],
    });
}

new Vue({
    el: "#app",
    render: h => h(App),
    router,
    store
});
