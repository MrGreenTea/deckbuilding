import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./components/Home.vue'),
    },
    {
      path: '/deck/:name',
      name: 'deck',
      component: () => import('./components/Deckbuilder.vue'),
    }
  ],
});

export default router;
