import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {};
const mutations = {};
const getters = {};
const actions = {};

const store = new Vuex.Store({
    state,
    mutations,
    actions,
    getters
});

export default store;
